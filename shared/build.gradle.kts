val koinVersion by extra("3.2.2")
val apolloVersion by extra("3.7.1")
val sqlDelightVersion by extra("1.5.4")
val napierVersion by extra("2.6.1")

plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("com.apollographql.apollo3") version "3.7.1"
    id("com.squareup.sqldelight")
    id("maven-publish")
}

group = "dev.ralphdugue.fanbrewdata"
version = "0.2.0"


sqldelight {
    database("Database") {
        packageName = "dev.ralphdugue.fanbrewdata.database"
    }
}

apollo {
    packageName.set("dev.ralphdugue.fanbrewdata")
}

kotlin {
    android {
        publishLibraryVariants("release", "debug")
    }
    
    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "ptt-data-layer"
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("com.apollographql.apollo3:apollo-runtime:$apolloVersion")
                implementation("io.insert-koin:koin-core:$koinVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
                implementation("com.squareup.sqldelight:runtime:$sqlDelightVersion")
                implementation("com.squareup.sqldelight:coroutines-extensions:$sqlDelightVersion")
                implementation("io.github.aakira:napier:$napierVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("com.squareup.sqldelight:android-driver:$sqlDelightVersion")
                implementation("io.insert-koin:koin-android:$koinVersion")
            }
        }
        val androidTest by getting
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            dependsOn(commonTest)
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
            dependencies {
                implementation("com.squareup.sqldelight:native-driver:$sqlDelightVersion")
            }
        }
    }
}

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("release") {
                artifactId = "android"
                from(components["release"])
            }
        }
        repositories {
            maven {
                url = uri("${System.getenv("CI_API_V4_URL")}/projects/41376646/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
            maven {
                name = "LocalRepo"
                url = uri(layout.buildDirectory.dir("repo"))
            }
        }
    }
}

android {
    namespace = "dev.ralphdugue.fanbrewdata"
    compileSdk = 33
    defaultConfig {
        minSdk = 23
        targetSdk = 33
        aarMetadata {
            minCompileSdk = 23
        }
    }
    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
}