package dev.ralphdugue.fanbrewdata.common

import com.apollographql.apollo3.api.ApolloResponse
import com.apollographql.apollo3.api.Operation
import com.apollographql.apollo3.exception.ApolloException
import io.github.aakira.napier.Napier

sealed interface ApiResult<T : Any>

class ApiSuccess<T : Any>(val data: T) : ApiResult<T>
class ApiError<T : Any>(val message: String?) : ApiResult<T>
class ApiException<T : Any>(val e: Throwable) : ApiResult<T>

inline fun <T : Operation.Data> safeApiCall(
    call: () -> ApolloResponse<T>
): ApiResult<T> {
    return try {
        val response = call()
        if (response.hasErrors()) {
            var errors = ""
            response.errors?.forEach { errors += "${it.message}\n" }
            Napier.d(errors)
            ApiError(errors)
        } else {
            ApiSuccess(response.dataAssertNoErrors)
        }
    } catch (e: ApolloException) {
        Napier.e("Apollo Query Exception", e)
        ApiException(e)
    }
}