package dev.ralphdugue.fanbrewdata

import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object PocketFanbrewRepos {

    object OPEN5E : KoinComponent {
        val classesRepository: ClassRepository by inject()
    }
}