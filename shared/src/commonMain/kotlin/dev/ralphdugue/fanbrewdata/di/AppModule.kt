package dev.ralphdugue.fanbrewdata.di

import com.apollographql.apollo3.ApolloClient
import com.squareup.sqldelight.db.SqlDriver
import dev.ralphdugue.fanbrewdata.database.Database
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassDataSource
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassDataSourceReal
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassRepository
import dev.ralphdugue.fanbrewdata.dnd5eAPI.classes.ClassRepositoryImpl
import org.koin.core.context.startKoin
import org.koin.dsl.module

interface DriverFactory {
    fun createDriver(): SqlDriver
}

val appModule = module {
   single {
       val url = "https://tabletop-api.ralphdugue.com"
       ApolloClient.Builder()
           .serverUrl(url)
           .build()
   }
    single<ClassDataSource> { ClassDataSourceReal(get(), get()) }
    single<ClassRepository> { ClassRepositoryImpl(get()) }
}

fun databaseModule(driverFactory: DriverFactory) = module {
    single { driverFactory.createDriver() }
    single { Database(get()) }
}

fun initBackend(driverFactory: DriverFactory){
    startKoin {
        modules(appModule + databaseModule(driverFactory))
    }
}