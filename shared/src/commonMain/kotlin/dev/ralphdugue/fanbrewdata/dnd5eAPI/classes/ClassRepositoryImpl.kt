package dev.ralphdugue.fanbrewdata.dnd5eAPI.classes

import dev.ralphdugue.fanbrewdata.ClassListQuery
import dev.ralphdugue.fanbrewdata.common.ApiResult
import dev.ralphdugue.fanbrewdata.common.ApiSuccess
import devralphduguefanbrewdata.classes.Class_list_class
import kotlinx.coroutines.flow.Flow

class ClassRepositoryImpl(private val classDataSource: ClassDataSource): ClassRepository {

    override suspend fun updateClassList(): ApiResult<ClassListQuery.Data> {
        return classDataSource.downloadAll().also { result ->
            when (result) {
                is ApiSuccess -> {
                    result.data.classesOpen5eFilterNotNull()?.let { classDataSource.insertAll(it) }
                }
                else -> {}
            }
        }
    }

    override fun watchClassList(): Flow<List<Class_list_class>> = classDataSource.retrieveAll()
}