package dev.ralphdugue.fanbrewdata.dnd5eAPI.classes


import dev.ralphdugue.fanbrewdata.ClassListQuery
import dev.ralphdugue.fanbrewdata.common.ApiResult
import devralphduguefanbrewdata.classes.Class_list_class
import kotlinx.coroutines.flow.Flow

interface ClassDataSource {
    suspend fun downloadAll(): ApiResult<ClassListQuery.Data>

    fun retrieveAll(): Flow<List<Class_list_class>>

    fun insertAll(data: List<ClassListQuery.ClassesOpen5e>)
}