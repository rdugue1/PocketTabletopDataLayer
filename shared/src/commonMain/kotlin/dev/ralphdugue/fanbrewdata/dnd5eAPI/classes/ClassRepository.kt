package dev.ralphdugue.fanbrewdata.dnd5eAPI.classes

import dev.ralphdugue.fanbrewdata.ClassListQuery
import dev.ralphdugue.fanbrewdata.common.ApiResult
import devralphduguefanbrewdata.classes.Class_list_class
import kotlinx.coroutines.flow.Flow

interface ClassRepository {

    suspend fun updateClassList(): ApiResult<ClassListQuery.Data>

    fun watchClassList(): Flow<List<Class_list_class>>
}