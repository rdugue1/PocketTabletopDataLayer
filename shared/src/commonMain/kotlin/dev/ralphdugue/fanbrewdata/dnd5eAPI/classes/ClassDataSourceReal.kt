package dev.ralphdugue.fanbrewdata.dnd5eAPI.classes

import com.apollographql.apollo3.ApolloClient
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import dev.ralphdugue.fanbrewdata.ClassListQuery
import dev.ralphdugue.fanbrewdata.common.safeApiCall
import dev.ralphdugue.fanbrewdata.database.Database
import devralphduguefanbrewdata.classes.Class_list_class
import kotlinx.coroutines.flow.Flow

class ClassDataSourceReal(
    private val apolloClient: ApolloClient,
    private val database: Database
): ClassDataSource {

    override suspend fun downloadAll() =  safeApiCall { apolloClient.query(ClassListQuery()).execute() }

    override fun retrieveAll(): Flow<List<Class_list_class>> =
        database.classListClassQueries.selectAll()
            .asFlow()
            .mapToList()

    override fun insertAll(data: List<ClassListQuery.ClassesOpen5e>) {
        data.forEach {
            database.classListClassQueries.insertClass(
                name = it.name ?: "",
                hit_dice = it.hit_dice ?: "",
                weapon_prof = it.prof_weapons,
                saving_throws = it.prof_saving_throws
            )
        }
    }

}